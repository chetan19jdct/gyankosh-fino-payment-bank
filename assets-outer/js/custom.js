$('#BannerSlider').owlCarousel({
    loop:true,
    margin:0,
    nav:true,
    autoplay:true,  
    smartSpeed: 1000,
    autoplayHoverPause:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }

     }, navText: ["<i class='fa fa-chevron-circle-left' aria-hidden='true'></i>","<i class='fa fa-chevron-circle-right' aria-hidden='true'></i>"] 

})


$(document).ready(function(){
  $("body").on('click', '.left-side-bar > ul > li > a', function(){
    $(".dropdown-show").slideUp();
    $('.fa.fa-angle-down').removeClass('arrowup');
    $(this).siblings(".dropdown-show").slideToggle();
    $(this).children('.fa.fa-angle-down').toggleClass('arrowup');
  });
});


$(document).ready(function(){
    $(".side-butn").on('click', function(){
    $(".left-side-bar").toggleClass("open-sidebar");
    $(this).toggleClass("middleclose");
    });
    $('body').on('click', '.left-side-bar.open-sidebar ul li a', function(){
        $(".left-side-bar").removeClass("open-sidebar");
        $('.side-butn').removeClass("middleclose");
    });
});

