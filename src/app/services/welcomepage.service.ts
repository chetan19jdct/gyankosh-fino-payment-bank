import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { LanguageService } from './language.service';

@Injectable({
  providedIn: 'root'
})
export class WelcomepageService {

  constructor(
    private httpService: HttpService,
    private languageService: LanguageService
  ) { }
  loadWelcomePageContent() {
    return this.httpService.get('ReadWelcomeContents?LanID=' + (this.languageService.selectedLanguage || '1'));
  }

}
