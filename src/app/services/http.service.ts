import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class HttpService {
  baseUrl: string = environment.baseUrl;

  constructor(private http: HttpClient) { }
  getHeaders() {
    const headers = new HttpHeaders();
    headers.append('Access-Control-Allow-Origin', '*');
    // headers.append('Access-Control-Allow-Origin', 'http://localhost:4200');

    return headers;
  }

  get(url: string) {
    return this.http.get(`${this.baseUrl}/${url}`, { headers: this.getHeaders() });
  }
  // post(url: string, data: any) {
  //   return this.http.post(`${this.baseUrl}/${url}`, data);
  // }
}
