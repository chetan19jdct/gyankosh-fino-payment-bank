import { TestBed } from '@angular/core/testing';

import { WelcomepageService } from './welcomepage.service';

describe('WelcomepageService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WelcomepageService = TestBed.get(WelcomepageService);
    expect(service).toBeTruthy();
  });
});
