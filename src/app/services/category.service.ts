import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { HttpClient,HttpHeaders, HttpParams } from '@angular/common/http';
import { map, filter, switchMap } from 'rxjs/operators';
import { LanguageService } from './language.service';
import 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class CategoryService {
apiurl; 
  constructor(
    private httpService: HttpService, 
    private http: HttpClient,
    private languageService: LanguageService
  ) {
    
    // this.apiurl = "http://10.15.20.113:8085/";
    // this.apiurl = "http://admin.pipasandesh.com/";
    // this.apiurl = "http://gyankosh.finopaymentbank.in:8085/";
    // this.apiurl = "http://netspacesoftware-001-site1.htempurl.com/";
    // this.apiurl = "http://netspacesoftware-001-site1.htempurl.com/admin/";
    // this.apiurl =  'https://gyankosh.finopaymentbank.in/admin/';
       this.apiurl = "https://10.15.20.113:8085/admin";
    

  }
  headerCommon() { 
    return {
    headers: new HttpHeaders({'Content-Type': 'application/x-www-form-urlencoded' })
    }; 
  }
  headerCommon1() {
    return {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    }; 
  }
  readAllGroup() {
    return this.httpService.get('ReadAllGroup?LanID=' + (this.languageService.selectedLanguage || '1'));
  }
  readAllMenu(){
    return this.httpService.get('ReadMenu?LanID=' + (this.languageService.selectedLanguage || '1'));
  }
  /* 
  readAllCategory() {
    return this.httpService.get('ReadAllGroup?LanID=' + (this.languageService.selectedLanguage || '1'));
  }  
  */
  readAllCategoryGroup(groupID){ 
    let params = new HttpParams().set('GroupID',groupID); 
      return this.http.post(this.apiurl+'/API/ReadListOfCategory?LanID=' + (this.languageService.selectedLanguage || '1'), params, this.headerCommon()).pipe(map((response: Response) => {
          return response;
      }
    ));
  } 
  readMessage(groupId,catId, subcatId){
    let params = new HttpParams().set('GroupID',groupId)
                                  .set('CatID',catId)
                                  .set('SubCatID',subcatId); 
            return this.http.post(this.apiurl+'/API/ReadPost?LanID=' + (this.languageService.selectedLanguage || '1'), params, this.headerCommon()).pipe(map((response: Response) => {
                    return response;
            }
    ));
  } 
  getSingleDetail(singleId) {
    let params = new HttpParams().set('PostId',singleId);
      return this.http.post(this.apiurl+'/API/ReadPostById', params, this.headerCommon()).pipe(map((response: Response) => {
          return response;
      }
    ));
  }
  entryValidation(value1) {
   let params = new HttpParams();
    /* let params = new HttpParams().set('RequestId','')
                                 .set('MethodId',null)
                                 .set('SessionId','f33b5528-cca4-4844-98a8-527bb20a7dc9')
                                 .set('TellerID','')
                                 .set('TokenId','')
                                 .set('X_Auth_Token','')
                                 .set('RequestData','')
                                 .set('IsEncrypt',false)
                                 .set('SessionExpiryTime','') */
                                
     /* 
      return this.http.post('http://webandappdevelopers.com/PAS/Webservice/AngularWebserviceCheck', params, this.headerCommon()).pipe(map((response: Response) => {
              return response;
        } 
      ));
    */
    return this.http.post(this.apiurl+'/API/CheckValidLoginSession', {"RequestId":"","MethodId":0,"SessionId":"f33b5528-cca4-4844-98a8-527bb20a7dc9","TellerID":"","TokenId":"","X_Auth_Token":"","RequestData":"","IsEncrypt":false,"SessionExpiryTime":""}, {headers: new HttpHeaders({'Content-Type':'application/json; charset=utf-8'})}).pipe(map((response: Response) => {
              return response;
          }  
      )); 
    }
    headerSearchResult (searchString){
      let params = new HttpParams().set('Title',searchString);
        return this.http.post(this.apiurl+'/API/ReadPostByTitle', params, this.headerCommon()).pipe(map((response: Response) => {
          return response;
        }
      ));
    }
}
