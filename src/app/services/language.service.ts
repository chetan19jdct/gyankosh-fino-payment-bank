import { Injectable, EventEmitter } from '@angular/core';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class LanguageService {
  onLanguageChange: EventEmitter<any> = new EventEmitter();
  selectedLanguage: string;

  constructor(private httpService: HttpService) {
    this.loadLanguage();
  }

  loadLanguage(): void {
    this.selectedLanguage = localStorage.getItem('selectedLanguage') || '1';
  }

  setLanguage(id: string): void {
    this.selectedLanguage = id;
    localStorage.setItem('selectedLanguage', id);
    this.onLanguageChange.emit();
  }

  readAllLanguage() {
    return this.httpService.get('ReadAllLanguage');
  }
  
}
