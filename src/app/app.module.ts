import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';
import { AppComponent } from './app.component';

// modules
import { AppRoutingModule } from './app-routing.module';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';

// components
import { HomeComponent } from './components/home/home.component';
import { FooterComponent } from './components/footer/footer.component';
import { WelcomepageComponent } from './components/welcomepage/welcomepage.component';
import { HeaderComponent } from './components/header/header.component';
import { FeedbackComponent } from './components/feedback/feedback.component';
import { ProductsalespitchComponent } from './components/productsalespitch/productsalespitch.component';
import { MarketingcollateralsComponent } from './components/marketingcollaterals/marketingcollaterals.component';
import { DeviceadminsupportComponent } from './components/deviceadminsupport/deviceadminsupport.component';
import { CustomergreetingsComponent } from './components/customergreetings/customergreetings.component';
import { CustomereducationComponent } from './components/customereducation/customereducation.component';
import { PagecomponentsComponent } from './components/pagecomponents/pagecomponents.component';
import { SinglePostComponent } from './components/single-post/single-post.component';
import { ErrorvalidationComponent } from './components/errorvalidation/errorvalidation.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    WelcomepageComponent,
    HomeComponent,
    FeedbackComponent,
    ProductsalespitchComponent,
    MarketingcollateralsComponent,
    DeviceadminsupportComponent,
    CustomergreetingsComponent,
    CustomereducationComponent,
    PagecomponentsComponent,
    SinglePostComponent,
    ErrorvalidationComponent,
 
  ],
  imports: [
    BrowserModule,
    RouterModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
  ],
  providers: [{provide: LocationStrategy, useClass: HashLocationStrategy}],
  bootstrap: [AppComponent]
})
export class AppModule { }
