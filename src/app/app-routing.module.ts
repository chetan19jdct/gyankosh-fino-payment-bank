import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { WelcomepageComponent } from './components/welcomepage/welcomepage.component';
import { HomeComponent } from './components/home/home.component';
import { FeedbackComponent } from './components/feedback/feedback.component';
import { ProductsalespitchComponent } from './components/productsalespitch/productsalespitch.component';
import { MarketingcollateralsComponent } from './components/marketingcollaterals/marketingcollaterals.component';
import { DeviceadminsupportComponent } from './components/deviceadminsupport/deviceadminsupport.component';
import { CustomergreetingsComponent } from './components/customergreetings/customergreetings.component';
import { CustomereducationComponent } from './components/customereducation/customereducation.component';
import { PagecomponentsComponent } from './components/pagecomponents/pagecomponents.component';
import { SinglePostComponent } from './components/single-post/single-post.component';
import { ErrorvalidationComponent } from './components/errorvalidation/errorvalidation.component';
import { AuthGuard } from './authguard/auth.guard';


const routes: Routes = [
  { path: '', component: WelcomepageComponent, pathMatch: 'full', canActivate: [AuthGuard]},
  { path: 'home', component: HomeComponent, pathMatch: 'full' },
  { path: 'feedback', component: FeedbackComponent},
  { path: 'Productsalespitch', component: ProductsalespitchComponent},
  { path: 'marketingcollaterals', component: MarketingcollateralsComponent},
  { path: 'deviceadminsupport', component: DeviceadminsupportComponent},
  { path: 'customergreetings', component: CustomergreetingsComponent},
  { path: 'customereducation', component: CustomereducationComponent},
  { path: 'group/:groupId', component: PagecomponentsComponent, canActivate: [AuthGuard]},
  { path: 'single/:singleId', component: SinglePostComponent, canActivate: [AuthGuard]},
  { path: 'invalidEntry', component: ErrorvalidationComponent},
  
  // last router 
  { path: '**', redirectTo: '/' }
];



@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  declarations: []
})
export class AppRoutingModule { }
