import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PagecomponentsComponent } from './pagecomponents.component';

describe('PagecomponentsComponent', () => {
  let component: PagecomponentsComponent;
  let fixture: ComponentFixture<PagecomponentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PagecomponentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PagecomponentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
