import { Component, OnInit } from '@angular/core';
import {Router,ActivatedRoute} from '@angular/router'; 
import { CategoryService } from 'src/app/services/category.service';
import { LanguageService } from 'src/app/services/language.service';


@Component({
  selector: 'app-pagecomponents',
  templateUrl: './pagecomponents.component.html',
  styleUrls: ['./pagecomponents.component.css']
})
export class PagecomponentsComponent implements OnInit {
  groupId = 0;
  allCategoryData;
  groupName ='';
  filtered;
  catPostData = {};
  objectKeys;
  sliderImg = [
    'assets/images/banner1.png',
    'assets/images/banner2.jpg',
    'assets/images/banner3.jpg',
    'assets/images/banner4.jpg'
  ];
  constructor(
    private activeRoute: ActivatedRoute,
    private categoryService: CategoryService,
    private languageService: LanguageService,
    private router: Router
  ) { 
      this.languageService.onLanguageChange.subscribe((status) => {
      this.ngOnInit();
      })
  }
  ngOnInit() {
    this.getParamaterLoadDetail();
    this.activeRoute.queryParams.subscribe(queryParams => {
      this.activeRoute.params.subscribe(routeParams => {
        this.loadGropdetail(routeParams.groupId);
        this.getParamaterLoadDetail();
      });
    });
  }
  getParamaterLoadDetail() {
    this.filtered = null;
    const routeParams = this.activeRoute.snapshot.params;
    this.groupId = routeParams.groupId;
    const groupId = this.groupId;
    this.loadGropdetail(this.groupId);
     this.categoryService.readAllGroup().subscribe((allGroup: any) => {
        this.filtered = allGroup.filter(function(item){
            return item.ID == groupId;         
        });
        this.groupName = this.filtered[0].Name;
        this.groupId = this.filtered[0].ID;
    })
  }
  loadGropdetail(groupId) {
    const subcatId = [] ; 
    this.categoryService.readAllCategoryGroup(groupId).subscribe((res) => {
        this.allCategoryData = res;
        
       this.allCategoryData.forEach((categoryData,key) => {
         this.catPostData[groupId] ={};
          if(categoryData.ListOfSubCategory) {
              categoryData.ListOfSubCategory.forEach((subcatId,key) => { 
                  this.categoryService.readMessage(groupId, categoryData.ID, subcatId.ID).subscribe((res) => {
                    this.catPostData[subcatId.ID] = res;
                   /*
                     const mapped = Object.keys( this.catPostData[subcatId.ID]).map(key => ({type: key, value:   this.catPostData[subcatId.ID][key]}));
                     this.catPostData[subcatId.ID] = mapped;
                   */
                  }) 
             }) 
          }
        })
      })
  } 
}
