import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductsalespitchComponent } from './productsalespitch.component';

describe('ProductsalespitchComponent', () => {
  let component: ProductsalespitchComponent;
  let fixture: ComponentFixture<ProductsalespitchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductsalespitchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductsalespitchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
