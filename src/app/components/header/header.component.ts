import { Component, OnInit,ElementRef,ViewChild,EventEmitter, Output } from '@angular/core';
import { CategoryService } from 'src/app/services/category.service';
import { LanguageService } from 'src/app/services/language.service';
import {Router,ActivatedRoute} from '@angular/router'; 
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  allCategoies = [];
  allLanguages = [];
  allCatSub ={}; 
  groupId;
  selectedLanguage: string;
  subCatElement = '';
  index;
  filtered;
  groupName;
  menuOption;
  headerAppendHtml;
  responses; 
  private element: HTMLElement;
  //@Output() getParamaterLoadDetail = new EventEmitter();

  @ViewChild('main') private content_elementRef: ElementRef;
  private content_element: HTMLElement;
  menuImg = [
    'assets/images/1.png',
  ];
  constructor(
    private activeRoute: ActivatedRoute,
    private categoryService: CategoryService,
    private languageService: LanguageService,
    private router: Router
  ) {
      this.languageService.onLanguageChange.subscribe((status) => {
      this.loadAllGroup();
      this.readAllMenu();
      const routeParams = this.activeRoute.snapshot.params;
      this.groupId = routeParams.groupId;
      if(this.groupId != '') {
        this.router.navigate(['/']);
      }
    })
  }
 ngOnInit() {
    

    this.selectedLanguage = this.languageService.selectedLanguage
    this.loadAllGroup();
    this.readAllMenu();
    this.loadLanguages();
}
/* testEvent() {
  this.getParamaterLoadDetail.emit();
} */
changeLanguage(): void {
    this.languageService.setLanguage(this.selectedLanguage);
}
loadLanguages(): void {
    this.languageService.readAllLanguage().subscribe((languages: any) => {
      this.allLanguages = languages;
   })
}
loadAllGroup(): void {
    this.categoryService.readAllGroup().subscribe((categoies: any) => {
      this.allCategoies = categoies;
          this.allCategoies.forEach((item, index) => {
          this.categoryService.readAllCategoryGroup(item.ID).subscribe((res) => {
            this.allCatSub[item.ID] = res
           })
      });
    })
  }
  readAllMenu(): void {
      this.categoryService.readAllMenu().subscribe((allMenu: any) => {
         this.menuOption = allMenu;
      })
  }
  getGroupCategory(groupId,_this): void {
   let floorElements = document.getElementsByClassName("inner_sub_category") as HTMLCollectionOf<HTMLElement>;
     for (var i=0;i<floorElements.length;i+=1){
        // floorElements[i].style.display = 'none';
      }
     const teshi = document.getElementsByClassName('inner_sub_category'+groupId) as HTMLCollectionOf<HTMLElement>;
     
     if(teshi[0].style.display == 'block') {
        teshi[0].style.display = 'none';
     } else {
        teshi[0].style.display = 'block';
     }
     //this.router.navigate(['/group/',groupId]);
  }
  serachHeader(ew) {
  this.categoryService.headerSearchResult(ew).subscribe((res) => {
        if(res) {
           this.responses = res;
           this.headerAppendHtml = "";
           this.responses.forEach((item, index) => {
             this.headerAppendHtml  += "<li> <a href='#/single/"+item.ID+"'>"+item.Title+"</a></li>"
            
          })
          const toggleBtn  = document.getElementsByClassName('dropdown-search') as HTMLCollectionOf<HTMLElement>;
          const toggleBtnHtml  = document.getElementsByClassName('headrSearchAppend') as HTMLCollectionOf<HTMLElement>;
          if(ew.length != 0) {
              toggleBtn[0].style.display = "block";  
              toggleBtnHtml[0].innerHTML = this.headerAppendHtml;
          } else {
            toggleBtn[0].style.display = "none";  
          
          }
        }
     })
  }
}
