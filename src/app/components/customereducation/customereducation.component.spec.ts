import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomereducationComponent } from './customereducation.component';

describe('CustomereducationComponent', () => {
  let component: CustomereducationComponent;
  let fixture: ComponentFixture<CustomereducationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomereducationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomereducationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
