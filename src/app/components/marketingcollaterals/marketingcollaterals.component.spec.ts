import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MarketingcollateralsComponent } from './marketingcollaterals.component';

describe('MarketingcollateralsComponent', () => {
  let component: MarketingcollateralsComponent;
  let fixture: ComponentFixture<MarketingcollateralsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MarketingcollateralsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MarketingcollateralsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
