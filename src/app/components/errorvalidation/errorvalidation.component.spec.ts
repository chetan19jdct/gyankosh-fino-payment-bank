import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ErrorvalidationComponent } from './errorvalidation.component';

describe('ErrorvalidationComponent', () => {
  let component: ErrorvalidationComponent;
  let fixture: ComponentFixture<ErrorvalidationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ErrorvalidationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ErrorvalidationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
