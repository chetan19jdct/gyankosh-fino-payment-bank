import { Component, OnInit } from '@angular/core';
import {Router,ActivatedRoute} from '@angular/router'; 
import { CategoryService } from 'src/app/services/category.service';
import { DomSanitizer } from '@angular/platform-browser';
@Component({
  selector: 'app-single-post',
  templateUrl: './single-post.component.html',
  styleUrls: ['./single-post.component.css']
})
export class SinglePostComponent implements OnInit {
  singleId;
  postDetail;
  postDescription;
  fileUrl = [];
  fileArrays;
  imgArrays;
  extrafileArrays;
  sliderImg = [
    'assets/images/banner1.png',
    'assets/images/banner2.jpg',
    'assets/images/banner3.jpg',
    'assets/images/banner4.jpg'
  ];
  constructor( private activeRoute: ActivatedRoute, 
              private categoryService: CategoryService,
              private sanitizer: DomSanitizer,
              private router: Router) {
               const routeParams = this.activeRoute.snapshot.params;
                this.singleId = routeParams.singleId;
                this.activeRoute.queryParams.subscribe(queryParams => {
                  this.activeRoute.params.subscribe(routeParams => {
                    this.singleDetail();
                    const toggleBtn  = document.getElementsByClassName('dropdown-search') as HTMLCollectionOf<HTMLElement>;
                    const toggleBtnHtml  = document.getElementsByClassName('headrSearchAppend') as HTMLCollectionOf<HTMLElement>;       
                    toggleBtn[0].style.display = "none"; 
                    toggleBtnHtml[0].innerHTML = "";
                  });
              });
            
  }
  ngOnInit() {
    this.singleDetail();
  }
  
  singleDetail() {
    const postFileUrl = '';
    var fileArray = [];
    var extrafileArray = [];
    var imgArray = [];
    var extensionImg ='';
    var extensionImg1 ='';
    var filename = '';
    var imagename = ''; 
    var filenameImg = "";
    var filenameExt = '';
    var imagenameExt = ''; 
   const routeParams = this.activeRoute.snapshot.params;
    this.singleId = routeParams.singleId;
    this.categoryService.getSingleDetail(this.singleId).subscribe((res) => {
    this.postDetail = res;
    this.postDescription =  this.postDetail.Description;
    this.postDescription = this.sanitizer.bypassSecurityTrustHtml(this.postDescription);
    /* files urls  */ 
     const postFileUrls  = res['ListOfFileName'];
     if(res['ListOfFileName']) {
          const count = 0;
            postFileUrls.forEach(function (value) {
            filename = value.split('/').pop()
              var extension = value.split('.').pop();
              var filetype = true;
              if(extension == 'mp4' || extension == 'avi' || extension == 'flv' || extension == 'mov') {
                  filetype = false;
              }
              switch(extension) {
                case 'docx':
                  extensionImg = 'assets/images/docx_logo.png';
                  break;
                case 'txt':
                  extensionImg = 'assets/images/text_icon_logo.png';
                  break;
                case 'pdf':
                  extensionImg = 'assets/images/pdf_logo.png';
                  break;  
                case 'PDF':
                  extensionImg = 'assets/images/pdf_logo.png';
                  break; 
                case 'pptx':
                  extensionImg = 'assets/images/ppt_logo.png';
                  break;   
                default:
                  extensionImg = 'assets/images/default_file_logo.png';
              }  
              fileArray.push({
                url: value,
                exten: extension,
                iconimage: extensionImg,
                filename: filename,
                filetype: filetype
              });
            });  
        }

      /* files urls  */
      /** extra files urls  */
      const postExtraFileUrls  = res['ListOfExtraFileName'];
      if(res['ListOfExtraFileName']) {
            postExtraFileUrls.forEach(function (value) {
              filenameExt = value.split('/').pop();
               var extension1 = value.split('.').pop();
               var filetype1 = true;
               if(extension1 == 'mp4' || extension1 == 'avi' || extension1 == 'flv' || extension1 == 'mov') {
                   filetype1 = false;
               }
               switch(extension1) {
                 case 'docx':
                   extensionImg1 = 'assets/images/docx_logo.png';
                   break;
                 case 'txt':
                   extensionImg1 = 'assets/images/text_icon_logo.png';
                   break;
                 case 'pdf':
                   extensionImg1 = 'assets/images/pdf_logo.png';
                   break;  
                 case 'PDF':
                   extensionImg1 = 'assets/images/pdf_logo.png';
                   break; 
                 case 'pptx':
                   extensionImg1 = 'assets/images/ppt_logo.png';
                   break;   
                 default:
                   extensionImg1 = 'assets/images/default_file_logo.png';
               }  
               extrafileArray.push({
                 url: value,
                 exten: extension1,
                 iconimage: extensionImg1,
                 filename: filenameExt,
                 filetype: filetype1
               });
             });  
         }
      /** extra files urls  end */  
      /* Images urls  */
     const postimagesUrls  = res['ListOfImageName'];
     if(res['ListOfImageName']){
        postimagesUrls.forEach(function (imgvalue) {


              imagename = imgvalue.split('/').pop();
              var extension2 = imgvalue.split('.').pop();
              var filetypeImg = true;
              if(extension2 == 'mp4' || extension2 == 'avi' || extension2 == 'flv' || extension2 == 'mov') {
                filetypeImg = false;
              }
              switch(extension2) {
                case 'docx':
                  extensionImg = 'assets/images/docx_logo.png';
                  break;
                case 'txt':
                  extensionImg = 'assets/images/text_icon_logo.png';
                  break;
                case 'pdf':
                  extensionImg = 'assets/images/pdf_logo.png';
                  break;  
                case 'PDF':
                  extensionImg = 'assets/images/pdf_logo.png';
                  break; 
                case 'pptx':
                  extensionImg = 'assets/images/ppt_logo.png';
                  break; 
                case 'jpeg':
                  extensionImg = imgvalue;
                  break;  
                case 'jpg':
                  extensionImg = imgvalue;
                  break; 
                case 'png':
                  extensionImg = imgvalue;
                  break;
                case 'png':
                  extensionImg = imgvalue;
                  break;    
                default:
                  extensionImg = 'assets/images/default_file_logo.png';
              }  
              imgArray.push({
                url: imgvalue,
                exten: extension2,
                iconimage: extensionImg,
                filename: imagename,
                filetype: filetypeImg
              });
          /* 
            imagename = imgvalue.split('/').pop();
            imgArray.push({
              url: imgvalue,
              filename: imagename
            }); 
            */
        })
      }
      /* Images urls end  */
    }
  )
  this.fileArrays = fileArray;
  this.imgArrays = imgArray;
  this.extrafileArrays = extrafileArray;
  
  }
  

  /* */
 
  /* */

}