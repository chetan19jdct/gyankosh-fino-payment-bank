import { Component, OnInit } from '@angular/core';
import { WelcomepageService } from 'src/app/services/welcomepage.service';
import { LanguageService } from 'src/app/services/language.service';

@Component({
  selector: 'app-welcomepage',
  templateUrl: './welcomepage.component.html',
  styleUrls: ['./welcomepage.component.css']
})
export class WelcomepageComponent implements OnInit {

  allwelcomecontent = [];
  selectedLanguage;

  constructor(
    private welcomepageService: WelcomepageService,
    private languageService: LanguageService
  ) {
    this.languageService.onLanguageChange.subscribe((status) => {
      this.loadWelcomePageContent();
    })
  }
  ngOnInit() {
    this.loadWelcomePageContent()
  }
  loadWelcomePageContent(): void {
    this.welcomepageService.loadWelcomePageContent().subscribe((languages: any) => {
      this.allwelcomecontent = languages;
    })
  }
}
