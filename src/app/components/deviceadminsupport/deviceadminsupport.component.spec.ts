import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeviceadminsupportComponent } from './deviceadminsupport.component';

describe('DeviceadminsupportComponent', () => {
  let component: DeviceadminsupportComponent;
  let fixture: ComponentFixture<DeviceadminsupportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeviceadminsupportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeviceadminsupportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
