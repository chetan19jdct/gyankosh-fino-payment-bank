import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomergreetingsComponent } from './customergreetings.component';

describe('CustomergreetingsComponent', () => {
  let component: CustomergreetingsComponent;
  let fixture: ComponentFixture<CustomergreetingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomergreetingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomergreetingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
