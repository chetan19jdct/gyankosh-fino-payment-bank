import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentpagesComponent } from './contentpages.component';

describe('ContentpagesComponent', () => {
  let component: ContentpagesComponent;
  let fixture: ComponentFixture<ContentpagesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContentpagesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentpagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
